package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	  
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher user = (Researcher) (request.getSession().getAttribute("user"));
		if((user != null) && (user.getId().equals("root"))) {
			Researcher n = new Researcher();
			n.setId(request.getParameter("id"));
			n.setName(request.getParameter("name"));
			n.setLastname(request.getParameter("lastname"));
			n.setEmail(request.getParameter("email"));
			n.setEmail(request.getParameter("password"));
			n.setEmail(request.getParameter("scopusURL"));			
			Client client = ClientBuilder.newClient(new ClientConfig());
			
			try {			
				
				Response newr = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/")
						  .request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(n,MediaType.APPLICATION_JSON));		  
					  getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request,response); 
				/*
				 * Researcher rtest =
				 * client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" +
				 * n.getId())
				 * .request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
				 */
			}			
			
			catch(Exception e){	
				
						request.setAttribute("message",
						  "No se puede crear Researcher");
						  getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request,
						  response); 				
			}			
						 
		}

	}
	
}
