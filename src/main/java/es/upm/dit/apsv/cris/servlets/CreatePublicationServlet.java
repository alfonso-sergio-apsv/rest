package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher user = (Researcher) (request.getSession().getAttribute("user"));
		if((user != null) && (user.getId().equals("root"))) {
			Publication p = new Publication();		
			p.setId(request.getParameter("id"));
			p.setTitle(request.getParameter("title"));
			p.setPublicationName(request.getParameter("publicationname"));
			p.setPublicationDate(request.getParameter("publicationdate"));
			p.setAuthors(request.getParameter("authors"));
			Client client = ClientBuilder.newClient(new ClientConfig());
			
			try {			
				
				Response newp = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/"+user.getId()+"/Publications")
						  .request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(p,MediaType.APPLICATION_JSON));		  
					  getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request,response); 
					  
					
				/*
				 * Researcher rtest =
				 * client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" +
				 * n.getId())
				 * .request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
				 */
			}			
			
			catch(Exception e){	
				
						request.setAttribute("message",
						  "No se puede crear la publicacion");
						  getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request,
						  response); 				
			}			
						 
		}

	}
	
}
