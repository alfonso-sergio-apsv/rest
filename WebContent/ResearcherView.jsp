<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Researcher</title>
</head>
<body>
<table>
<tr>
           	  <td>${ri.id}</td>
              <td>${ri.name}</td>
              <td>${ri.lastname}</td>
              <td><a href="${ri.scopusURL}">${ri.scopusURL}</a></td>
              <td>${ri.email}</td>
</tr>
</table>

<table>
<tr>
<th>Publication</th>
</tr>

<c:forEach items="${publications}" var="pi">
        <tr>
			<td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
        </tr>
</c:forEach>

</table>	
<!-- <c:if test="${ri.id == user.id}"> -->
		<h2>Create Publication</h2>
	<form action="CreatePublicationServlet" method="post">
		<h2>Create Publication</h2>
		<p>
			ID: <input type="text" name="id" placeholder="Publication Id"/>
		</p>
		<p>
			Título: <input type="text" name="title" placeholder="Publication Title"/>
		</p>
		<p>
			Nombre de la Publicación: <input type="text" name="publicationName" placeholder="Publication Name" />
		</p>
		<p>
			Fecha de la publicación: <input type="text" name="publicationDate" placeholder="Publication Date"/>
		</p>
		<p>
			Autor: <input type="text" name="authors" placeholder="Publication Authors ID"/>
		</p>
		<p>
			<button type="submit">Crear Publicación</button>
		</p>
	</form>
	<!-- </c:if> -->
</body>
</html>